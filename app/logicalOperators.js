exports = (typeof window === 'undefined') ? global : window;

exports.logicalOperatorsAnswers = {
  or : function(a, b) {
      //if( typeof a === "boolean") {
      //  if (typeof b === "boolean")
      //    return a | b;
      //}
      return a || b;
  },

  and : function(a, b) {
    //if( typeof a === "boolean") {
    //  if (typeof b === "boolean")
    //    return a & b;
    //}
    return a && b;
  }
};
