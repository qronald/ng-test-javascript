exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
    var i,val = -1,pos=1000;
    for(i=0;i < arr.length;i++) {
      if (item == arr[i]) {
        val = arr[i];
        pos = i;
        break;
      } else {
        pos = -1;
      }

    }
    return pos;
  },

  sum : function(arr) {
    var i,s = 0;
    for(i=0; i < arr.length;i++) {
      s += arr[i];
    }
    return s;
  },

  remove : function(arr, item) {
    var i;
    for(i=0; i < arr.length;i++) {
      if (item === arr[i]){
        delete arr[i];
        arr.splice(i, 1);
      }
    }
    return arr;
  },

  removeWithoutCopy : function(arr, item) {
    for (var index = 0; index < arr.length; index++) {
      if (arr[index] === item) {
        arr.splice(index, 1);
        index--;
      }
    }
    return arr;
  },

  append : function(arr, item) {
    arr.push(item);
    return arr;
  },

  truncate : function(arr) {
    arr.pop();
    return arr;
  },

  prepend : function(arr, item) {
    arr.unshift(item);
    return arr;
  },

  curtail : function(arr) {
    arr.shift();
    return arr;
  },

  concat : function(arr1, arr2) {
    return arr1.concat(arr2);
  },

  insert : function(arr, item, index) {
    arr.splice(index, 0, item);
    return arr;
  },

  count : function(arr, item) {
    var i,c = 0;
    for(i=0; i < arr.length;i++) {
      if (arr[i] === item){
        c++;
      }
    }
    return c;
  },

  duplicates : function(arr) {
    var item, ar = [];
    for (var index = 0; index < arr.length; index++) {
      item = arr[index];
      var du =false;
      for (var j = index+1; j < arr.length; j++) {
        if (arr[j] === item) {
          du = true;
          arr.splice(j, 1);
          j--;
        }
      }
      if (du) {
        ar.push(item);
      }
    }
    return ar;
  },

  square : function(arr) {
    for(var i = 0;i < arr.length;i++) {
      arr[i] = (arr[i] * arr[i]);
    }
    return arr;
  },

  findAllOccurrences : function(arr, target) {
    var i,ar = [];
    for(i=0;i < arr.length;i++) {
      if (arr[i] === target){
        ar.push(i);
      }
    }
    return ar;
  }
};
