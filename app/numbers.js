exports = (typeof window === 'undefined') ? global : window;

exports.numbersAnswers = {
  valueAtBit: function(num, bit) {

  },

  base10: function(str) {
    return parseInt(str,2);
  },

  convertToBinary: function(num) {
    var st = num.toString(2);
    if(st.length < 8){
      st = st.split("");
      for (var i=7-st.length;i >= 0 ;i--) {
        st.unshift('0');
      }
      st = st.join("");
    }
    return st;
  },

  multiply: function(a, b) {
    var number = a*b;
    var multiplier = Math.pow(10, 4);
    return (Math.round(number * multiplier) / multiplier);
  }
};
