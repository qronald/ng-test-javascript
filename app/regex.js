exports = (typeof window === 'undefined') ? global : window;

exports.regexAnswers = {
  containsNumber : function(str) {
      // 'abc123' should return true
      // 'abc' should return false
    var patron = /\d$/;
    if(str.search(patron)>0)
      return true;
    else
      return false;
  },

  containsRepeatingLetter : function(str) {
      // 'aabc' should return true
      // 'abc' should return false
    //var patron = /^[a-zA-Z]{2}$/;///[a-zA-Z]{2}/;
    //console.log(str);
    //console.log(str.search(patron));
    //if(str.search(patron))
    //  return true;
    //else
    //  return false;
  },

  endsWithVowel : function(str) {
      // 'something' should return false
      // 'create' should return true
    if (str.search(/[aeiouAEIOU]$/) > 0) {
      return true;
    } else {
      return false;
    }
  },

  isUSD : function(str) {
      // for example:
      // '$132.03' should be true
      // '$132,219' should be true
      // '$132,212.43' should be true
      // '$132.034,233' should be false
      // '$132.034_23' should be false
      // '$132.013,14.23' should be false
    var patron = /^\$(((\d{1,3},)(\d{3},)*\d{3})|(\d{1,3}))\.\d{2}$/;
    console.log (str.search(patron)) ;
    if(str.search(patron)>0)
      return true;
    else
      return false;
  }
};
