exports = (typeof window === 'undefined') ? global : window;

exports.modulesAnswers = {
  createModule : function(str1, str2) {
    //return {
    //  name : str2,
    //  greeting: str1,
    //  sayIt: function () {
    //    var that = this;
    //    return that.name + ', '+ that.greeting;
    //  }
    //};

    var r ={};
    r.name = str2;
    r.greeting = str1;
    r.sayIt= function (){
      return r.greeting+", "+r.name;
    };
    return r;
  }
};
