exports = (typeof window === 'undefined') ? global : window;

exports.functionsAnswers = {
  argsAsArray : function(fn, arr) {
    /*
      Helpers
      fn = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
    */
    return fn(arr[0],arr[1],arr[2]);
  },

  speak : function(fn, obj) {
    /*
      Helpers 
      var sayIt = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };   
      var fn = function() {
          return sayIt(this.greeting, this.name, '!!!');
        };
     */
    var n= {};
    n.fun = fn;
    n.greeting = obj.greeting;
    n.name = obj.name;
    return n.fun();
  },

  functionFunction : function(str) {
    var str1;
    var fn1= function (string) {
      str1 = str + string;
      return str;
    };
    return str1;
  },

  makeClosures : function(arr, fn) {

  },

  partial : function(fn, str1, str2) {
    /**
     * Helpers
     * fn = function(greeting, name, punctuation) {
        sayItCalled = true;
        return greeting + ', ' + name + (punctuation || '!');
      };
     */
  },

  useArguments : function() {
    var i, sum = 0;
    for (i = 0; i < arguments.length; i += 1) {
      sum += arguments[i];
    }
    return sum;
  },

  callIt : function(fn) {

  },

  partialUsingArguments : function(fn) {
    /*
      Helpers     
      var partialMe = function (x, y, z) {
        return x / y * z;
      };
     */
  },

  curryIt : function(fn) {
    /**
     * Helpers
     * var fn = function (x, y, z) {
        return x / y * z;
      };
     */
  }
};
